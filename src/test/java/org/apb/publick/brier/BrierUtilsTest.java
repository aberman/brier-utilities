/**
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
 * DEALINGS IN THE SOFTWARE.                                                 
 */
package org.apb.publick.brier;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * @author andrewberman
 */
public class BrierUtilsTest extends TestCase {

  private static final BrierUtils T = BrierUtils.get();

  private static final double P = 0;
  private static final double E = 0.000000000000001;

  public BrierUtilsTest(String testName) {
    super(testName);
  }

  public static Test suite() {
    return new TestSuite(BrierUtilsTest.class);
  }

  public void testGetBooleanBrier() {
    Assert.assertEquals(0.0, T.getBooleanBrier(1, true), P);
    Assert.assertEquals(0.0, T.getBooleanBrier(0, false), P);
    Assert.assertEquals(1.0, T.getBooleanBrier(0, true), P);
    Assert.assertEquals(1.0, T.getBooleanBrier(1, false), P);
    Assert.assertEquals(0.25, T.getBooleanBrier(0.5, true), E);
    Assert.assertEquals(0.25, T.getBooleanBrier(0.5, false), E);
    Assert.assertEquals(0.04, T.getBooleanBrier(0.8, true), E);
    Assert.assertEquals(0.04, T.getBooleanBrier(0.2, false), E);
    Assert.assertEquals(0.64, T.getBooleanBrier(0.8, false), E);
    Assert.assertEquals(0.64, T.getBooleanBrier(0.2, true), E);
  }

  public void testGetCompatibleBooleanBrier() {
    Assert.assertEquals(0.0, T.getCompatibleBooleanBrier(1, true), P);
    Assert.assertEquals(0.0, T.getCompatibleBooleanBrier(0, false), P);
    Assert.assertEquals(2.0, T.getCompatibleBooleanBrier(0, true), P);
    Assert.assertEquals(2.0, T.getCompatibleBooleanBrier(1, false), P);
    Assert.assertEquals(0.5, T.getCompatibleBooleanBrier(0.5, true), E);
    Assert.assertEquals(0.5, T.getCompatibleBooleanBrier(0.5, false), E);
    Assert.assertEquals(0.08, T.getCompatibleBooleanBrier(0.8, true), E);
    Assert.assertEquals(0.08, T.getCompatibleBooleanBrier(0.2, false), E);
    Assert.assertEquals(1.28, T.getCompatibleBooleanBrier(0.8, false), E);
    Assert.assertEquals(1.28, T.getCompatibleBooleanBrier(0.2, true), E);
  }

  public void testGetAccuracy() {
    Assert.assertEquals(0.0, T.getAccuracy(new double[] {}, new double[] {}, 1.0), E);
    Assert.assertEquals(0.275, T.getAccuracy(new double[] { 0.5 }, new double[] { 0.3, 0.6 }, 0.5), E);
    Assert.assertEquals(-0.03216666666666668,
        T.getAccuracy(new double[] { 0.2, 0.5, 0.6 }, new double[] { 0.1, 0.53, 0.8, 0.91 }, 0.7), E);
    Assert.assertEquals(0.2928571428571429, T.getAccuracy(new double[] { 0.8, 0.4, 0.2, 0.8, 0.1, 0.5, 0.3 },
        new double[] { 0.1, 0.3, 0.5, 0.7, 1.2, 1.3 }, 0.25), E);
    Assert.assertEquals(0.9725, T.getAccuracy(new double[] { 1.2, 1.3 }, new double[] { 0.01, 1.1 }, 0.5), E);
    Assert.assertEquals(0.6666666666666667,
        T.getAccuracy(new double[] { 1.2, 0.7, 1.6 }, new double[] { 0.9, 1.1 }, 0.5), E);
    Assert.assertEquals(0.016666666666666607,
        T.getAccuracy(new double[] { 2.0, 1.0, 0.8 }, new double[] { 0.5, 1.2, 1.3, 1.5 }, 1.0), E);
    Assert.assertEquals(0.2925,
        T.getAccuracy(new double[] { 0.3, 0.3, 0.3 }, new double[] { 0.3, 0.4, 1.1, 1.2 }, 0.01), E);
  }

  public void testGetRankedProbabilityScore() {
    Assert.assertEquals(0.0, T.getRankedProbabilityScore(new double[] { 1.0 }, 0), E);
    Assert.assertEquals(2.0, T.getRankedProbabilityScore(new double[] { 0.0, 1.0 }, 0), E);
    Assert.assertEquals(0.0, T.getRankedProbabilityScore(new double[] { 0.0, 1.0 }, 1), E);
    Assert.assertEquals(0.0, T.getRankedProbabilityScore(new double[] { 1.0, 0.0 }, 0), E);
    Assert.assertEquals(2.0, T.getRankedProbabilityScore(new double[] { 1.0, 0.0 }, 1), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 0.5, 0.0, 0.5 }, 0), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 0.5, 0.0, 0.5 }, 1), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 0.5, 0.0, 0.5 }, 2), E);
    Assert.assertEquals(0.6, T.getRankedProbabilityScore(new double[] { 0.2, 0.3, 0.4, 0.1 }, 0), E);
    Assert.assertEquals(0.2, T.getRankedProbabilityScore(new double[] { 0.2, 0.3, 0.4, 0.1 }, 1), E);
    Assert.assertEquals(0.2, T.getRankedProbabilityScore(new double[] { 0.2, 0.3, 0.4, 0.1 }, 2), E);
    Assert.assertEquals(0.8666666666666667, T.getRankedProbabilityScore(new double[] { 0.2, 0.3, 0.5, 0.1 }, 3), E);
    Assert.assertEquals(0.0, T.getRankedProbabilityScore(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 }, 0), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 }, 1), E);
    Assert.assertEquals(1.0, T.getRankedProbabilityScore(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 }, 2), E);
    Assert.assertEquals(1.5, T.getRankedProbabilityScore(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 }, 3), E);
    Assert.assertEquals(2.0, T.getRankedProbabilityScore(new double[] { 1.0, 0.0, 0.0, 0.0, 0.0 }, 4), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 0.0, 1.0, 0.0, 0.0, 0.0 }, 2), E);
    Assert.assertEquals(0.0, T.getRankedProbabilityScore(new double[] { 0.0, 0.0, 1.0, 0.0, 0.0 }, 2), E);
    Assert.assertEquals(0.5, T.getRankedProbabilityScore(new double[] { 0.0, 0.0, 0.0, 1.0, 0.0 }, 2), E);
    Assert.assertEquals(1.0, T.getRankedProbabilityScore(new double[] { 0.0, 0.0, 0.0, 0.0, 1.0 }, 2), E);
  }

  public void testAverage() {
    Assert.assertEquals(0, T.getAverage(new double[0]), P);
    Assert.assertEquals(2, T.getAverage(new double[] { 2 }), P);
    Assert.assertEquals(1.5, T.getAverage(new double[] { 2, 1 }), E);
    Assert.assertEquals(1.5, T.getAverage(new double[] { 1, 2 }), E);
    Assert.assertEquals(2.0, T.getAverage(new double[] { 1, 2, 3 }), E);
    Assert.assertEquals(2.0, T.getAverage(new double[] { 4, 2, 0 }), E);
    Assert.assertEquals(2.0, T.getAverage(new double[] { 2, 2, 2 }), E);
  }

  public void testMedian() {
    Assert.assertEquals(0.0, T.getMedian(new double[0]), P);
    Assert.assertEquals(26.7, T.getMedian(new double[] { 26.7 }), P);
    Assert.assertEquals(1.5, T.getMedian(new double[] { 1.0, 2.0 }), E);
    Assert.assertEquals(1.5, T.getMedian(new double[] { 2.0, 1.0 }), E);
    Assert.assertEquals(1.23, T.getMedian(new double[] { -3, 1.23, 8 }), P);
    Assert.assertEquals(1.23, T.getMedian(new double[] { 8, -3, 1.23 }), P);
    Assert.assertEquals(1.23, T.getMedian(new double[] { -3, 8, 1.23 }), P);
    Assert.assertEquals(2.5, T.getMedian(new double[] { 0, 2, 3, 80 }), E);
    Assert.assertEquals(2.5, T.getMedian(new double[] { 80, 0, 3, 2 }), E);
    Assert.assertEquals(2.5, T.getMedian(new double[] { 2, 80, 0, 3 }), E);
    Assert.assertEquals(1.0, T.getMedian(new double[] { 0, 0, 1, 5, 5 }), P);
    Assert.assertEquals(1.0, T.getMedian(new double[] { 1, 1, 1, 1, 1 }), P);
  }

}
