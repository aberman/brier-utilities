/**
 *                                                                            
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
 * DEALINGS IN THE SOFTWARE.                                                 
 *                                                                           
 */
package org.apb.publick.brier;

import java.util.Arrays;

/**
 * A collection of utilities for Brier scores. From the wikipedia entry: "The Brier score is a proper score function
 * that measures the accuracy of probabilistic predictions. It is applicable to tasks in which predictions must assign
 * probabilities to a set of mutually exclusive discrete outcomes."
 * 
 * @author andrewberman
 */
public class BrierUtils {

  private static final BrierUtils INSTANCE = new BrierUtils();

  public static BrierUtils get() {
    return INSTANCE;
  }

  /**
   * Returns a Brier score for an estimation of a boolean experiment, doubled for compatibility with multi-choice Brier
   * scores.
   * 
   * @param estimation
   *          P(x)
   * @param isTrue
   *          true if x is true
   * @return the Brier score, doubled.
   */
  public double getCompatibleBooleanBrier(double estimation, boolean isTrue) {
    return 2.0 * getBooleanBrier(estimation, isTrue);
  }

  /**
   * Returns a Brier score for an estimation of a boolean experiment.
   * 
   * @param estimation
   *          P(x)
   * @param isTrue
   *          true if x is true
   * @return the Brier score
   */
  public double getBooleanBrier(double estimation, boolean isTrue) {
    return Math.pow(estimation - (isTrue ? 1.0 : 0.0), 2);
  }

  /**
   * Returns an accuracy score for a single participant compared to the brier scores of a population. Defined as the
   * median crowd brier score subtracted from the average participant brier score multiplied by the participation rate
   * of the participant.
   * 
   * @param participantBriers
   *          the scores of the participant being evaluated
   * @param populationBriers
   *          the scores of the entire population
   * @param participationRate
   *          the fraction of the potention questions answered by the participant
   * @return the accuracy score
   */
  public double getAccuracy(double[] participantBriers, double[] populationBriers, double participationRate) {
    return (getAverage(participantBriers) - (getMedian(populationBriers)) * participationRate);
  }

  /*
   * Returns the Ranked Probability Score for an ordered multiple choice question. For example, if the choices range
   * from 1 to 5 and the correct choice is 3, estimations for 2 or 4 get more credit than estimations for 1 or 5.
   *
   */
  public double getRankedProbabilityScore(double[] estimations, int correctChoice) {
    if (estimations.length == 1) {
      return getCompatibleBooleanBrier(estimations[0], true);
    }
    double totalLeft = 0;
    double totalBrier = 0;
    for (int i = 0; i < estimations.length; i++) {
      totalLeft += estimations[i];
      double totalRight = 1.0 - totalLeft;
      double leftValue = (correctChoice <= i ? 1. : 0.);
      double rightValue = 1 - leftValue;
      double brierSub = Math.pow((totalLeft - leftValue), 2) + Math.pow((totalRight - rightValue), 2);
      totalBrier += brierSub;
    }
    return totalBrier / (estimations.length - 1.0);
  }

  /**
   * Returns the median value. Avoids use of apache math commons library. Returns 0 for empty arrays.
   * 
   * @param cp
   *          an array of values
   * @return the median, or average of lower and upper median if an even number of entries
   */
  public double getMedian(double[] cp) {
    int len = cp.length;
    if (len == 0) {
      return 0;
    }
    Arrays.sort(cp);
    if (len % 2 == 0) {
      return (0.5 * (cp[len / 2] + cp[len / 2 - 1]));
    }
    return cp[len / 2];
  }

  /**
   * Returns the average. Avoids use of apache math commons library. Returns 0 for empty arrays.
   * 
   * @param cp
   *          an array of values
   * @return the average
   */
  public double getAverage(double[] cp) {
    int len = cp.length;
    if (len == 0) {
      return 0;
    }
    double s = 0;
    for (int i = 0; i < cp.length; i++) {
      s += cp[i];
    }
    return s / len;
  }

}
